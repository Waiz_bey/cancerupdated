﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hmsvid
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void btnAddPatient_Click(object sender, EventArgs e)
        {
            labelind1.ForeColor = System.Drawing.Color.Red;
            labelind2.ForeColor = System.Drawing.Color.Black;
            labelind3.ForeColor = System.Drawing.Color.Black;
            labelind4.ForeColor = System.Drawing.Color.Black;
            labelind5.ForeColor = System.Drawing.Color.Black;
            labelind6.ForeColor = System.Drawing.Color.Black;
            panel1.Visible = true;
            panel2.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
        }

        private void btnGeneralPhysicalExam_Click(object sender, EventArgs e)
        {
            labelind2.ForeColor = System.Drawing.Color.Red;
            labelind1.ForeColor = System.Drawing.Color.Black;
            
            labelind3.ForeColor = System.Drawing.Color.Black;
            labelind4.ForeColor = System.Drawing.Color.Black;
            labelind5.ForeColor = System.Drawing.Color.Black;
            labelind6.ForeColor = System.Drawing.Color.Black;
            panel2.Visible = true;
            panel1.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
        }

        private void btnPreOrderDiagnosis_Click(object sender, EventArgs e)
        {
            labelind3.ForeColor = System.Drawing.Color.Red;
            labelind1.ForeColor = System.Drawing.Color.Black;
            labelind2.ForeColor = System.Drawing.Color.Black;
            
            labelind4.ForeColor = System.Drawing.Color.Black;
            labelind5.ForeColor = System.Drawing.Color.Black;
            labelind6.ForeColor = System.Drawing.Color.Black;
            panel3.Visible = true;
            panel2.Visible = false;
            panel1.Visible = false;
            pictureBox2.Visible = false;
            panel4.Visible = false;


        }

        private void btnDiagnosis_Click(object sender, EventArgs e)
        {
            labelind4.ForeColor = System.Drawing.Color.Red;
            labelind1.ForeColor = System.Drawing.Color.Black;
            labelind2.ForeColor = System.Drawing.Color.Black;
            labelind3.ForeColor = System.Drawing.Color.Black;
            
            labelind5.ForeColor = System.Drawing.Color.Black;
            labelind6.ForeColor = System.Drawing.Color.Black;
        }

        private void btnTreatment_Click(object sender, EventArgs e)
        {
            labelind5.ForeColor = System.Drawing.Color.Red;
            labelind1.ForeColor = System.Drawing.Color.Black;
            labelind2.ForeColor = System.Drawing.Color.Black;
            labelind3.ForeColor = System.Drawing.Color.Black;
            labelind4.ForeColor = System.Drawing.Color.Black;
            
            labelind6.ForeColor = System.Drawing.Color.Black;
            panel3.Visible = false;
            panel2.Visible = false;
            panel1.Visible = false;
            panel4.Visible = true;
        }

        private void btnFullHistoryofPatient_Click(object sender, EventArgs e)
        {
            labelind6.ForeColor = System.Drawing.Color.Red;
            labelind1.ForeColor = System.Drawing.Color.Black;
            labelind2.ForeColor = System.Drawing.Color.Black;
            labelind3.ForeColor = System.Drawing.Color.Black;
            labelind4.ForeColor = System.Drawing.Color.Black;
            labelind5.ForeColor = System.Drawing.Color.Black;
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            panel1.Visible = false;
            comboBoxCity.AutoCompleteMode = AutoCompleteMode.Suggest;
            comboBoxCity.AutoCompleteSource = AutoCompleteSource.ListItems;
            comboboxBloodGroup.AutoCompleteMode = AutoCompleteMode.Suggest;
            comboboxBloodGroup.AutoCompleteSource = AutoCompleteSource.ListItems;
            comboBoxProvince.AutoCompleteMode = AutoCompleteMode.Suggest;
            comboBoxProvince.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                string name = txtPatientName.Text;
                string area = txtArea.Text;
                string refferedBy = txtRefferedBy.Text;
                Int64 contactNo = Convert.ToInt64(txtPatientContactNo.Text);
                Int64 CNIC = Convert.ToInt64(txtCNIC.Text);
                int patientAge = Convert.ToInt32(txtPatientAge.Text);
                string gender = comboBoxGender.Text;
                string bloodGroup = comboboxBloodGroup.Text;
                string Province = comboBoxProvince.Text;
                string city = comboBoxCity.Text;

                SqlConnection con = new SqlConnection();
                con.ConnectionString = "data source = DESKTOP-MHP090T\\Aatir Ahmed;database = hospital;integrated security =True";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "Insert into AddPatient values ('" + refferedBy + "'," + CNIC + "," + contactNo + ",'" + name + "'," + patientAge + ", '" + gender + "', '" + bloodGroup + "', '" + Province + "', '" + city + "', '" + area + "')";
                SqlDataAdapter DA = new SqlDataAdapter(cmd);
                DataSet DS = new DataSet();
                DA.Fill(DS);
            }
            catch(Exception)
            {
                MessageBox.Show("Data not Saved ");
            }
        MessageBox.Show("Data Saved");
        
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
